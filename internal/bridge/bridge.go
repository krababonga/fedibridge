package bridge

import (
	"context"
	"fmt"
	"time"

	"github.com/pkg/errors"
)

const (
	SuccessAccountCreateMessage = "OK"
	NotFoundMessage             = "NotFound"
)

var ErrNotFound = errors.New(NotFoundMessage)

type QueueProvider interface {
	SendMessageToQueue(ctx context.Context, queueName, message string) error
	GetMessageFromQueue(queueName string, timeout time.Duration) (string, error)
}

type QueueAccountResolver struct {
	provider            QueueProvider
	accountsQueue       string
	responseQueuePrefix string
	responseTimeout     time.Duration
}

func Make(queueProvider QueueProvider, accountsQueue, responseQueuePrefix string,
	responseTimeout time.Duration) *QueueAccountResolver {
	return &QueueAccountResolver{
		provider:            queueProvider,
		accountsQueue:       accountsQueue,
		responseQueuePrefix: responseQueuePrefix,
		responseTimeout:     responseTimeout,
	}
}

func (q *QueueAccountResolver) CreateAccount(ctx context.Context, username string) (string, error) {
	if err := q.provider.SendMessageToQueue(ctx, q.accountsQueue, username); err != nil {
		return "", errors.Wrap(err, "unable to send message with queue provider")
	}

	msg, err := q.provider.GetMessageFromQueue(fmt.Sprintf("%s%s", q.responseQueuePrefix, username), q.responseTimeout)
	if err != nil {
		return "", errors.Wrap(err, "unable to get message from queue provider")
	}

	if msg == NotFoundMessage {
		return "", ErrNotFound
	}

	return msg, nil
}
