package queue

import (
	"context"
	"fmt"
	"time"

	"github.com/pkg/errors"
	amqp "github.com/rabbitmq/amqp091-go"
)

var ErrTimeout = errors.New("timeout reached")

type Config struct {
	Host     string
	Port     int
	Username string
	Password string
}

type Rabbit struct {
	conn *amqp.Connection
}

func Make(cnf *Config) (*Rabbit, error) {
	conn, err := amqp.Dial(fmt.Sprintf("amqp://%s:%s@%s:%d/", cnf.Username, cnf.Password, cnf.Host, cnf.Port))
	if err != nil {
		return nil, errors.Wrap(err, "unable to connect to rabbit")
	}

	return &Rabbit{
		conn: conn,
	}, nil
}

func (r *Rabbit) SendMessageToQueue(ctx context.Context, queueName, message string) error {
	ch, err := r.conn.Channel()
	if err != nil {
		return errors.Wrap(err, "unable to init rabbit channel")
	}
	defer ch.Close()

	q, err := ch.QueueDeclare(
		queueName, // name
		false,     // durable
		false,     // delete when unused
		false,     // exclusive
		false,     // no-wait
		nil,       // arguments
	)

	if err != nil {
		return errors.Wrap(err, "unable to declare queue")
	}

	err = ch.PublishWithContext(ctx, "", q.Name, false, // mandatory
		false, // immediate
		amqp.Publishing{
			ContentType: "text/plain",
			Body:        []byte(message),
		})

	if err != nil {
		return errors.Wrap(err, "unable to publish message")
	}

	return nil
}

func (r *Rabbit) GetMessageFromQueue(queueName string, timeout time.Duration) (string, error) {
	ch, err := r.conn.Channel()
	if err != nil {
		return "", errors.Wrap(err, "unable to init rabbit channel")
	}
	defer ch.Close()

	q, err := ch.QueueDeclare(
		queueName, // name
		false,     // durable
		false,     // delete when unused
		false,     // exclusive
		false,     // no-wait
		nil,       // arguments
	)

	msgs, err := ch.Consume(
		q.Name, // queue
		"",     // consumer
		true,   // auto-ack
		false,  // exclusive
		false,  // no-local
		false,  // no-wait
		nil,    // args
	)

	select {
	case m := <-msgs:
		return string(m.Body), nil
	case <-time.After(timeout):
		return "", ErrTimeout
	}
}
