package testrig

import (
	"context"

	"github.com/superseriousbusiness/gotosocial/internal/processing"
)

type testBridge struct{}

func (testBridge) CreateAccount(ctx context.Context, username string) (string, error) {
	return "", nil
}

func NewTestBridge() processing.Bridge {
	return testBridge{}
}
